#include <vector>
#include <string>
#include <exception>
#include <iostream>
#include <stdexcept>
#include "FileHandler.h"
#include <tbb/parallel_for.h>
#include <tbb/blocked_range.h>
#include <tbb/blocked_range2d.h>

typedef std::vector<std::vector<int>> matrix;

bool check_matrices_for_multiplication(matrix& m1, matrix& m2) {
	int col_count1 = m1[0].size();

	int row_count2 = m2.size();

	if (col_count1 == row_count2) {
		// matrices can be multiplied if and only if their dimensions are:
		// m x n ; n x p ==> m x p
		// ie number of columns of the first matrix must be equal to number of rows of the second matrix
		return true;
	}
	return false;
}

matrix result;
class parallel_multiplier {
private:
	int row_count1;
	int col_count1;
	int row_count2;
	int col_count2;
	matrix m1;
	matrix m2;


public:
	// initialize object atributes
	parallel_multiplier(matrix& mat1, matrix& mat2) {
		row_count1 = mat1.size();
		col_count1 = mat1[0].size();
		row_count2 = mat2.size();
		col_count2 = mat2[0].size();
		m1 = mat1;
		m2 = mat2;
		for (int i = 0; i < row_count1; ++i) {
			result.push_back(std::vector<int>(col_count2, 0));
		}
	}
	
	// used by tbb::parallel_for
	void operator()(const tbb::blocked_range<int> range) const {
		for (int i = range.begin(); i < range.end(); ++i) {
			for (int j = 0; j < col_count2; ++j) {
				for (int x = 0; x < row_count2; ++x) {
					result[i][j] += m1[i][x] * m2[x][j];
				}
			}
		}
	}

	// used by tbb::parallel_for
	void operator()(const tbb::blocked_range2d<int, int> range) const {
		for (int i = range.rows().begin(); i < range.rows().end(); ++i) {
			for (int j = range.cols().begin(); j < range.cols().end(); ++j) {
				for (int x = 0; x < row_count2; ++x) {
					result[i][j] += m1[i][x] * m2[x][j];
				}
			}
		}
	}
};

bool compare_with_serial_solution(matrix& result) {
	// loads matrix from serial solution and compares it with result matrix gotten from parallel_for
	std::string path("./../Serial Solution/serial_result_matrix_" + std::to_string(result.size()) + "x" + std::to_string(result[0].size()) + ".txt");
	matrix serial_solution = filehandler::load_matrix(path);
	unsigned int i, j;
	for (i = 0; i < result.size(); ++i) {
		for (j = 0; j < result[0].size(); ++j) {
			if (result[i][j] != serial_solution[i][j]) { return false; }
		}
	}
	return true;
}

int main() {
	try {

		//-------------------------- loading matrices for multiplication ---------------------------------------------------------------------------

		std::string left_matrix("left_matrix_500x500.txt");
		std::string right_matrix("right_matrix_500x500.txt");
		auto start = std::chrono::high_resolution_clock::now();
		matrix mat1 = filehandler::load_matrix("./../test files/" + left_matrix);
		matrix mat2 = filehandler::load_matrix("./../test files/" + right_matrix);
		auto finish = std::chrono::high_resolution_clock::now();
		auto duration1 = std::chrono::duration_cast<std::chrono::seconds>(finish - start);
		std::cout << "Time spent loading the files: " << duration1.count() << "seconds" << std::endl;
		
		if (!check_matrices_for_multiplication(mat1, mat2)) {
			throw std::runtime_error("Matrices have invalid dimensions for multiplication.");
		}
		
		//-------------------------- calls parallel for with 1dimensional range ---------------------------------------------------------------------

		parallel_multiplier pm(mat1, mat2);
		
		start = std::chrono::high_resolution_clock::now();
		tbb::parallel_for(tbb::blocked_range<int>(0, (int)mat1.size()), pm);
		finish = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
		std::cout << "1drange Multiplication time: " << duration.count() << "microseconds" << std::endl;
		
		//--------------------------/ OMITTED / calls parallel for with 2dimensional range / OMITTED / ----------------------------------------------

		/*result.clear();
		tbb::blocked_range2d<int, int> range2d(0, (int)mat1.size(), 0, (int)mat2[0].size());
		parallel_multiplier pm2(mat1, mat2);

		start = std::chrono::high_resolution_clock::now();
		tbb::parallel_for(range2d, pm2);
		finish = std::chrono::high_resolution_clock::now();
		duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
		std::cout << "2drange Multiplication time: " << duration.count() << "microseconds" << std::endl;
		
		---- It is usually slower than 1d range parallel_for (From my testing).
		*/

		//-------------------------- checks the result ----------------------------------------------------------------------------------------------

		if (!compare_with_serial_solution(result)) {
			throw std::runtime_error("Result isn't equal with Serial Solution. It is incorrect.");
		}
		else {
			std::cout << "Result is equal to Serial Solution. All is good." << std::endl;
		}

		//---------------------- records the results and execution time in txt files ----------------------------------------------------------------

		filehandler::record_result(result);
		filehandler::record_multiplication_time(std::to_string(duration.count()), left_matrix, right_matrix);
	}
	catch (std::runtime_error e) {
		std::cout << e.what() << std::endl;
	}

	return 0;
}