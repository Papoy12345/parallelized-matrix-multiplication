#pragma once
#include <fstream>
#include <ostream>
#include "FileHandler.h"
#include "Mapper.h"

matrix filehandler::load_matrix(std::string path) {
	// loads txt file and packs data (numbers) into matrix
	std::ifstream file(path);
	std::string rowData;
	matrix m;
	while (getline(file, rowData)) {
		m.push_back(mapper::split(rowData, " "));
		if (!mapper::is_matrix_valid(m)) {
			file.close();
			throw std::runtime_error("Matrix has improper number of collumns in a row");
		}
	}
	file.close();
	return m;
}

void filehandler::record_result(matrix& result) {
	//writes down the result matrix
	int rows = result.size();
	int cols = result[0].size();
	std::ofstream new_record("task_group_result_matrix_" + std::to_string(rows) + "x" + std::to_string(cols) + ".txt");
	std::string record_txt;
	for (int row = 0; row < rows; ++row) {
		for (int col = 0; col < cols; ++col) {
			record_txt += std::to_string(result[row][col]) + " ";
		}
		record_txt = record_txt.substr(0, record_txt.size() - 1); //deletes last space " " in the row
		record_txt += "\n";
	}
	new_record << record_txt;
	new_record.close();
}

void filehandler::record_multiplication_time(std::string microseconds_res1, std::string microseconds_res2, std::string microseconds_res3,
	std::string left_matrix, std::string right_matrix) {
	//writes down how long it took for multiplication to finish, for each task_group implementation
	std::string filename("task_group_multiplication_time.txt");
	std::ofstream file_out;

	file_out.open(filename, std::ios_base::app);
	std::string text("Left matrix: " + left_matrix + "\n" + "Right matrix: " + right_matrix + "\n");
	text += "(One task group doing one element) Multiplication time: " + microseconds_res1 + " microseconds\n";
	text += "(One task group doing one row) Multiplication time: " + microseconds_res2 + " microseconds\n";
	text += "(Number of task groups == number of hyperthreads) Multiplication time: " + microseconds_res3 + " microseconds\n\n";

	file_out << text << std::endl;
}