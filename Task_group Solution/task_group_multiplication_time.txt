Left matrix: left_matrix_500x500.txt
Right matrix: right_matrix_500x500.txt
(One task group doing one element) Multiplication time: 5383647 microseconds
(One task group doing one row) Multiplication time: 4633479 microseconds
(Number of task groups == number of hyperthreads) Multiplication time: 6845533 microseconds


Left matrix: left_matrix_700x700.txt
Right matrix: right_matrix_700x700.txt
(One task group doing one element) Multiplication time: 14540321 microseconds
(One task group doing one row) Multiplication time: 13949491 microseconds
(Number of task groups == number of hyperthreads) Multiplication time: 20459592 microseconds


Left matrix: left_matrix_4x3.txt
Right matrix: right_matrix_3x4.txt
(One task group doing one element) Multiplication time: 1724 microseconds
(One task group doing one row) Multiplication time: 97 microseconds
(Number of task groups == number of hyperthreads) Multiplication time: 236 microseconds


Left matrix: left_matrix_100x93.txt
Right matrix: right_matrix_93x101.txt
(One task group doing one element) Multiplication time: 68984 microseconds
(One task group doing one row) Multiplication time: 40208 microseconds
(Number of task groups == number of hyperthreads) Multiplication time: 60356 microseconds


Left matrix: left_matrix_100x93.txt
Right matrix: right_matrix_93x101.txt
(One task group doing one element) Multiplication time: 90417 microseconds
(One task group doing one row) Multiplication time: 38232 microseconds
(Number of task groups == number of hyperthreads) Multiplication time: 57995 microseconds


Left matrix: left_matrix_500x500.txt
Right matrix: right_matrix_500x500.txt
(One task group doing one element) Multiplication time: 5241038 microseconds
(One task group doing one row) Multiplication time: 4521710 microseconds
(Number of task groups == number of hyperthreads) Multiplication time: 6665506 microseconds


Left matrix: left_matrix_500x500.txt
Right matrix: right_matrix_500x500.txt
(One task group doing one element) Multiplication time: 5314392 microseconds
(One task group doing one row) Multiplication time: 4537816 microseconds
(Number of task groups == number of hyperthreads) Multiplication time: 6812245 microseconds


