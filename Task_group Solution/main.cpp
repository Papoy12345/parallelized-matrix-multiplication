#include <vector>
#include <string>
#include <exception>
#include <thread>
#include <iostream>
#include <stdexcept>
#include "FileHandler.h"
#include "tbb/task_group.h"
#include <chrono>
#include <math.h>


typedef std::vector<std::vector<int>> matrix;

bool check_matrices_for_multiplication(matrix& m1, matrix& m2) {
	int col_count1 = m1[0].size();

	int row_count2 = m2.size();

	if (col_count1 == row_count2) {
		// matrices can be multiplied if and only if their dimensions are:
		// m x n ; n x p
		// ie number of columns of the first matrix must be equal to number of rows of the second matrix
		return true;
	}
	return false;
}

void do_one_element(int row, int collumn, matrix& mat1, matrix& mat2, matrix& result) {
	int row_count2 = mat2.size();
	// serial code where row and collumn are predetermined
	for (int x = 0; x < row_count2; x++) {
		result[row][collumn] += mat1[row][x] * mat2[x][collumn];
	}
}

void do_one_row(int row, matrix& mat1, matrix& mat2, matrix& result) {
	int row_count2 = mat2.size();
	int col_count2 = mat2[0].size();
	// serial code where row is already predetermined
	for (int j = 0; j < col_count2; j++) {
		for (int x = 0; x < row_count2; x++) {
			result[row][j] += mat1[row][x] * mat2[x][j];
		}
	}
}

void do_hyperthread_task(int start_row, int start_col, int end_row, int end_col,
	matrix& mat1, matrix& mat2, matrix& result) {
	int row_count1 = result.size();
	int col_count2 = result[0].size();
	
	// it is possible for passed parameters to exceed maximum row and collumn count
	// because steps are consts (number of collumns/rows may not be divisible with step)
	if (end_row > row_count1) { end_row = row_count1; }
	if (end_col > col_count2) { end_col = col_count2; }

	for (int i = start_row; i < end_row; i++)
	{
		for (int j = start_col; j < end_col; j++)
		{
			for (int x = 0; x < (int)mat2.size(); x++)
			{
			result[i][j] += mat1[i][x] * mat2[x][j];
			}
		}
	}
}

matrix task_group_one_el(matrix& mat1, matrix& mat2) {
	int row_count1 = mat1.size();
	int col_count1 = mat1[0].size();
	int row_count2 = mat2.size();
	int col_count2 = mat2[0].size();
	matrix result;

	for (int i = 0; i < row_count1; ++i) {
		result.push_back(std::vector<int>(col_count2, 0));
	}

	tbb::task_group g;
	// by serializing 2 for loops, each task will calculate 1 element
	for (int i = 0; i < row_count1; i++) {
		for (int j = 0; j < col_count2; j++) {
			g.run([&, i, j] { do_one_element(i, j, mat1, mat2, result); });
		}
	}
	g.wait();
	return result;
}

matrix task_group_one_row(matrix& mat1, matrix& mat2) {
	matrix result;
	int row_count1 = mat1.size();
	int col_count2 = mat2[0].size();
	for (int i = 0; i < row_count1; ++i) {
		result.push_back(std::vector<int>(col_count2, 0));
	}

	tbb::task_group g;
	// by serializing 1st for loop, and pushing other two in task_group object
	// each task will calculate 1 row.
	for (int i = 0; i < row_count1; i++) {
		g.run([&, i] { do_one_row(i, mat1, mat2, result); }); // bestcase: number of threads == row_count1
	}
	g.wait();
	return result;
}

matrix task_group_hyperthreads(matrix& mat1, matrix& mat2) {
	// creates number of tasks that is same (or similar) to number of CPU cores (hyperthreads)
	matrix result;
	int row_count1 = mat1.size();
	int col_count2 = mat2[0].size();
	for (int i = 0; i < row_count1; ++i) {
		result.push_back(std::vector<int>(col_count2, 0));
	}
	const unsigned int num_of_elements = row_count1 * col_count2;
	const unsigned int processor_count = std::thread::hardware_concurrency();
	//calculates number of result matrix elements that single task will be in charge of
	unsigned int task_size = num_of_elements / processor_count;
	
	int start_row = 0;
	int start_col = 0;
	const int step_by_row = sqrt(task_size); 
	const int step_by_col = sqrt(task_size);
	// by making step the size of square root of task_size, (nearly) every task group
	// will calculate a submatrix that is the size step_by_row x step_by_col
	int end_row = start_row + step_by_row;
	int end_col = start_col + step_by_col;

	tbb::task_group g;
	while (start_row < row_count1) {

		while (start_col < col_count2) {
			g.run([&, start_row, start_col, end_row, end_col]
				{do_hyperthread_task(start_row, start_col, end_row, end_col, mat1, mat2, result); });
			// move the collumn, row remains the same
			start_col = end_col;
			end_col = start_col + step_by_col;
		}
		// reset the collumn, move the row
		start_row = end_row;
		end_row = start_row + step_by_row;
		start_col = 0;
		end_col = start_col + step_by_col;

	}
	g.wait();
	return result;
}

bool compare_results(matrix& res1, matrix& res2, matrix& res3) {
	// compares result matrices from all task_group implementations
	try {
		unsigned int i, j;
		for (i = 0; i < res1.size(); ++i) {
			for (j = 0; j < res1[0].size(); ++j) {
				if (res1[i][j] != res2[i][j]) { return false; }
			}
		}
		// if first matrix is equal with other two, then they are all equal with each other
		for (i = 0; i < res1.size(); ++i) {
			for (j = 0; j < res1[0].size(); ++j) {
				if (res1[i][j] != res3[i][j]) { return false; }
			}
		}
	}
	catch (std::out_of_range e) {
		// this one shouldn't happen due to multiple previous checks
		throw std::runtime_error("One of the result matrices is invalid dimensions");
	}
	return true;
}

bool compare_with_serial_solution(matrix& result) {
	// loads serial solution matrix and compares it with result gotten from task_groups.
	// it is previously confirmed that all task_group results are equal before the call of this function 
	std::string path("./../Serial Solution/serial_result_matrix_" + std::to_string((int)result.size()) + "x" + std::to_string((int)result[0].size()) + ".txt");
	matrix serial_solution = filehandler::load_matrix(path);
	unsigned int i, j;
	for (i = 0; i < result.size(); ++i) {
		for (j = 0; j < result[0].size(); ++j) {
			if (result[i][j] != serial_solution[i][j]) { return false; }
		}
	}
	return true;
}

int main() {
	try {

		//-------------------------- loading matrices for multiplication ---------------------------------------------------------------------------

		std::string left_matrix("left_matrix_500x500.txt");
		std::string right_matrix("right_matrix_500x500.txt");

		auto start = std::chrono::high_resolution_clock::now();
		matrix mat1 = filehandler::load_matrix("./../test files/" + left_matrix);
		matrix mat2 = filehandler::load_matrix("./../test files/" + right_matrix);
		auto finish = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::seconds>(finish - start);
		std::cout << "Time spent loading the files: " << duration.count() << "seconds" << std::endl;

		if (!check_matrices_for_multiplication(mat1, mat2)) 
		{
			throw std::runtime_error("Matrices have invalid dimensions for multiplication.");
		}

		//-------------------------- task group where every task calculates one element ------------------------------------------------------------

		start = std::chrono::high_resolution_clock::now();
		matrix result1 = task_group_one_el(mat1, mat2);
		finish = std::chrono::high_resolution_clock::now();
		auto duration1 = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
		std::cout << "(One task group doing one element) Multiplication time: " << duration1.count() << "microseconds" << std::endl;
		
		//-------------------------- task group where every task calculates one row ----------------------------------------------------------------
		
		start = std::chrono::high_resolution_clock::now();
		matrix result2 = task_group_one_row(mat1, mat2);
		finish = std::chrono::high_resolution_clock::now();
		auto duration2 = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
		std::cout << "(One task group doing one row) Multiplication time: " << duration2.count() << "microseconds" << std::endl;
		
		//------------------------- task group where number of tasks is equal to (or similar) to number of CPU cores (hyperthreads) ----------------
		
		start = std::chrono::high_resolution_clock::now();
		matrix result3 = task_group_hyperthreads(mat1, mat2);
		finish = std::chrono::high_resolution_clock::now();
		auto duration3 = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
		std::cout << "(Number of task groups == number of hyperthreads) Multiplication time: " << duration3.count() << "microseconds" << std::endl;
		
		//------------------------- calls comaprison functions to check if resulting matrices are correct ------------------------------------------
		
		if (!compare_results(result1, result2, result3)) {
			throw std::runtime_error("Result matrices aren't equal.");
		}
		else {
			std::cout << "All resulting matrices are equal." << std::endl;
		}

		if (!compare_with_serial_solution(result1)) {
			throw std::runtime_error("Result isn't equal with Serial Solution. It is incorrect.");
		}
		else {
			std::cout << "Result is equal to Serial Solution. All is good." << std::endl;
		}

		//---------------------- records the results and execution time in txt files ----------------------------------------------------------------

		std::string microseconds_res1 = std::to_string(duration1.count());
		std::string microseconds_res2 = std::to_string(duration2.count());
		std::string microseconds_res3 = std::to_string(duration3.count());
		filehandler::record_result(result1);
		filehandler::record_multiplication_time(microseconds_res1, microseconds_res2, microseconds_res3, left_matrix, right_matrix);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	}

	return 0;
}