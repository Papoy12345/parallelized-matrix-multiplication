import random


def generate_file(file, rows, collumns):
	matrix = ""
	row = []
	for i in range(rows):
		for j in range(collumns):
			row.append(str(random.randint(0, 10)))
		matrix += " ".join(row) + "\n"
		row.clear()
	file.write(matrix)
	file.close()


if __name__ == '__main__':
	m100x93 = open("left_matrix_100x93.txt", "w")
	m93x101 = open("right_matrix_93x101.txt", "w")
	m500x500 = open("left_matrix_500x500.txt", "w")
	_m500x500 = open("right_matrix_500x500.txt", "w")
	m700x700 = open("left_matrix_700x700.txt", "w")
	_m700x700 = open("right_matrix_700x700.txt", "w")

	generate_file(m100x93, 100, 93)
	generate_file(m93x101, 93, 101)
	generate_file(m500x500, 500, 500)
	generate_file(_m500x500, 500, 500)
	generate_file(m700x700, 700, 700)
	generate_file(_m700x700, 700, 700)
