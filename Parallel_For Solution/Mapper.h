#pragma once

#include <vector>
#include <string>
#include <tbb/concurrent_vector.h>

typedef std::vector<std::vector<int>> matrix;

namespace mapper {
	bool is_matrix_valid(matrix& m);
	std::vector<int> split(std::string& rowData, std::string delimiter);
}