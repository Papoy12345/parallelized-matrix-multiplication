#pragma once
#include <vector>
#include <string>

typedef std::vector<std::vector<int>> matrix;

namespace filehandler {
	matrix load_matrix(std::string path);
	void record_result(matrix result);
	void record_multiplication_time(std::string microseconds, std::string left_matrix, std::string right_matrix);
}