#pragma once

#include "Mapper.h"

bool mapper::is_matrix_valid(matrix& m) {
	if (m.size() == 1) {
		// there is only one row in matrix, then it is valid;
		return true;
	}
	if (m[m.size() - 1].size() != m[m.size() - 2].size()) {
		// compares the number of collumns in newly added row with previous one;
		// if sizes aren't the same, it means matrix is invalid;
		// example: [[1, 2, 3] , [4, 5]] is invalid matrix;
		return false;
	}
	return true;
}

std::vector<int> mapper::split(std::string& rowData, std::string delimiter)
{    // splits the given line of text by the delimiter and returns vector of values between delimiters

	std::vector<int> mat_row;

	int start_pos = 0;
	int end_pos = rowData.find(delimiter);

	while (end_pos != rowData.npos) {
		mat_row.push_back(std::stoi(rowData.substr(start_pos, end_pos - start_pos)));
		start_pos = end_pos + delimiter.length();
		end_pos = rowData.find(delimiter, start_pos);
	}
	mat_row.push_back(std::stoi(rowData.substr(start_pos, end_pos - start_pos)));
	return mat_row;
}