#include <vector>
#include <string>
#include <exception>
#include <iostream>
#include <stdexcept>
#include "FileHandler.h"
#include <chrono>

typedef std::vector<std::vector<int>> matrix;

bool check_matrices_for_multiplication(matrix& m1, matrix& m2) {
	int col_count1 = m1[0].size();

	int row_count2 = m2.size();

	if (col_count1 == row_count2) {
		// matrices can be multiplied if and only if their dimensions are:
		// m x n ; n x p
		// ie number of columns of the first matrix must be equal to number of rows of the second matrix
		return true;
	}
	return false;
}

void serial_multiply(matrix& result, matrix& mat1, matrix& mat2, int row_count1, int col_count2, int row_count2)
{
	
	int x, i, j;

	for (i = 0; i < row_count1; i++)
	{
		for (j = 0; j < col_count2; j++)
		{
			for (x = 0; x < row_count2; x++)
			{
				result[i][j] += mat1[i][x] * mat2[x][j];
			}
		}
	}
}

void print_result(matrix& result) {
	std::cout << "Resulting matrix is:" << std::endl;
	for (int i = 0; i < result.size(); ++i) {
		for (int j = 0; j < result[0].size(); ++j) {
			std::cout << result[i][j] << " ";
		}
		std::cout << std::endl;
	}
}

int main()
{
	try {

		// ------------------------------ loads file ------------------------------------------

		std::string left_matrix("left_matrix_700x700.txt");
		std::string right_matrix("right_matrix_700x700.txt");
		
		auto start = std::chrono::high_resolution_clock::now();
		matrix mat1 = filehandler::load_matrix("E:/VisualStudio Projects/PP projekat/test files/" + left_matrix);
		matrix mat2 = filehandler::load_matrix("E:/VisualStudio Projects/PP projekat/test files/" + right_matrix);
		auto finish = std::chrono::high_resolution_clock::now();
		auto duration1 = std::chrono::duration_cast<std::chrono::seconds>(finish - start);
		std::cout << "Time spent loading the files: " << duration1.count() << "seconds" << std::endl;

		if (!check_matrices_for_multiplication(mat1, mat2)) {
			throw std::runtime_error("Matrices have invalid dimensions for multiplication.");
		}

		// ------------------------------- sets up result matrix ------------------------------

		int row_count1 = mat1.size();
		int col_count1 = mat1[0].size();
		int row_count2 = mat2.size();
		int col_count2 = mat2[0].size();
		matrix result;
		for (int i = 0; i < row_count1; i++)
		{
			std::vector<int> newvec(col_count2, 0);
			result.push_back(newvec);
		}

		// ---------------------------- serial multiplication ----------------------------------

		start = std::chrono::high_resolution_clock::now();
		serial_multiply(result, mat1, mat2, row_count1, col_count2, row_count2);
		finish = std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(finish - start);
		std::cout << "Multiplication time: " << duration.count() << "microseconds" << std::endl;

		// ---------------------------- write down result in a txt file ------------------------
		
		filehandler::record_result(result);
		filehandler::record_multiplication_time(std::to_string(duration.count()), left_matrix, right_matrix);
	}
	catch (std::runtime_error e) {
		std::cout << e.what() << std::endl;
	}
	
	return 0;
}