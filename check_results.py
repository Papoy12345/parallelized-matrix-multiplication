if __name__ == '__main__':
    serial_m100x101 = open("Paralelno Programiranje Projekat1/serial_result_matrix_100x101.txt", "r")
    serial_m500x500 = open("Paralelno Programiranje Projekat1/serial_result_matrix_500x500.txt", "r")
    serial_m700x700 = open("Paralelno Programiranje Projekat1/serial_result_matrix_700x700.txt", "r")

    s_m100x101 = serial_m100x101.read()
    s_m500x500 = serial_m500x500.read()
    s_m700x700 = serial_m700x700.read()

    parallel_for_m100x101 = open("Parallel_For Solution/parallel_for_result_matrix_100x101.txt", "r")
    parallel_for_m500x500 = open("Parallel_For Solution/parallel_for_result_matrix_500x500.txt", "r")
    parallel_for_m700x700 = open("Parallel_For Solution/parallel_for_result_matrix_700x700.txt", "r")

    p_m100x101 = parallel_for_m100x101.read()
    p_m500x500 = parallel_for_m500x500.read()
    p_m700x700 = parallel_for_m700x700.read()

    task_m100x101 = open("Task_group Solution/task_group_result_matrix_100x101.txt", "r")
    task_m500x500 = open("Task_group Solution/task_group_result_matrix_500x500.txt", "r")
    task_m700x700 = open("Task_group Solution/task_group_result_matrix_700x700.txt", "r")

    t_m100x101 = task_m100x101.read()
    t_m500x500 = task_m500x500.read()
    t_m700x700 = task_m700x700.read()

    if s_m100x101 == p_m100x101:
        print("parallel for: OK\t100x101")
    else:
        print("parallel for: NOT OK\t100x101")
    if s_m500x500 == p_m500x500:
        print("parallel for: OK\t500x500")
    else:
        print("parallel for: NOT OK\t500x500")
    if s_m700x700 == p_m700x700:
        print("parallel for: OK\t700x700")
    else:
        print("parallel for: NOT OK\t700x700")


    if s_m100x101 == t_m100x101:
        print("task group: OK\t100x101")
    else:
        print("task group: NOT OK\t100x101")
    if s_m500x500 == t_m500x500:
        print("task group: OK\t500x500")
    else:
        print("task group: NOT OK\t500x500")
    if s_m700x700 == t_m700x700:
        print("task group: OK\t700x700")
    else:
        print("task group: NOT OK\t700x700")
